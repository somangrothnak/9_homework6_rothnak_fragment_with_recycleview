package com.somee.rothnaksomang.homework6_fragmentlist_recycleview;

public interface Callback {
    void replaceFragment(University university);
}
