package com.somee.rothnaksomang.homework6_fragmentlist_recycleview;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class ItemViewFragment extends Fragment {

//  declare variable
    private University university;
    public ItemViewFragment(){}

    public void setUniversity(University university){
        this.university=university;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//      inflater xml file to object
        View view=LayoutInflater.from(getContext()).inflate(R.layout.layout_item,container,false);

        ItemViewHolder itemViewHolder=new ItemViewHolder(view);

//      set value to view in Item view fragment
        if(itemViewHolder!=null){
            itemViewHolder.tvName.setText(university.getName());
            itemViewHolder.tvCall.setText(university.getPhone());
            itemViewHolder.tvWebsite.setText(university.getWebsite());
            itemViewHolder.tvMail.setText(university.getMail());
            itemViewHolder.tvAddress.setText(university.getAddress());
            itemViewHolder.ivProfile.setImageResource(university.getProfile());
            itemViewHolder.ivProfileSmall.setImageResource(university.getProfile());
        }
        return view;
    }

//  class holder view in item view
    class ItemViewHolder extends RecyclerView.ViewHolder {

        TextView tvName,tvCall,tvWebsite,tvMail,tvAddress;
        ImageView ivProfile,ivProfileSmall;
        CardView cvItem;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
//          referent variable to view in xml file
            tvName=itemView.findViewById(R.id.tv_name);
            tvCall=itemView.findViewById(R.id.tv_call);
            tvWebsite=itemView.findViewById(R.id.tv_web);
            tvMail=itemView.findViewById(R.id.tv_mail);
            tvAddress=itemView.findViewById(R.id.tv_address);
            ivProfile=itemView.findViewById(R.id.iv_profile_big);
            cvItem=itemView.findViewById(R.id.cv_item_university);
            ivProfileSmall=itemView.findViewById(R.id.iv_profile_small);
        }
    }

}
