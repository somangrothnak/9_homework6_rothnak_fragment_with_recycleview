package com.somee.rothnaksomang.homework6_fragmentlist_recycleview;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity implements Callback{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//      create a object from RecycleViewFragment and
//      add this fragment to main activity
        RecycleViewFragment recycleViewFragment=new RecycleViewFragment();
        setDefaultFragment(recycleViewFragment);

    }
//  replace Fragment Method
    public void replaceFragment(Fragment destFragment){
        FragmentManager fragmentManager=this.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fl_parent,destFragment);

        //add to back stack
        //for make activity don't finish
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

//    Method set Defualt Fragment
    public void setDefaultFragment(Fragment defaultFragment){
        replaceFragment(defaultFragment);
    }


//  Method replaceFragment from Interface Callback
    @Override
    public void replaceFragment(University university) {
        ItemViewFragment itemViewFragment=new ItemViewFragment();
//      callback function
        itemViewFragment.setUniversity(university);
        replaceFragment(itemViewFragment);
    }
}
