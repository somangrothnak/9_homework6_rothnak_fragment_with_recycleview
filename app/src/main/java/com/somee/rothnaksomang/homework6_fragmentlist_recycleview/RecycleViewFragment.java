package com.somee.rothnaksomang.homework6_fragmentlist_recycleview;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class RecycleViewFragment extends Fragment {

//  declare variable
    private ArrayList<University> universities;
    RecyclerView recyclerView;
    UniversityAdapter universityAdapter;
    View view;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        universities=new ArrayList<>();
//      call method to set value to recyclerView
        setData();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
         view=LayoutInflater.from(getContext()).inflate(R.layout.layout_recycleview,container,false);
//       set reference to view at xml file
         recyclerView=view.findViewById(R.id.rv_university);
//       create object of UniversityAdapter
         universityAdapter=new UniversityAdapter(getContext(),universities);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//      set value to View on recycle view
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(universityAdapter);
    }

    public void setData(){
            universities.add(new University("Korea Software HRD","098","hrd.com","hrd@gmail","st007",R.drawable.u0));
            universities.add(new University("RUPP","098","rupp.com","rupp@gmail","st001",R.drawable.u1));
            universities.add(new University("PUC","098","puc.com","puc@gmail","st002",R.drawable.u2));
            universities.add(new University("NORTON","098","norton.com","norton@gmail","st003",R.drawable.u3));
            universities.add(new University("UC","098","uc.com","uc@gmail","st012",R.drawable.u4));
            universities.add(new University("RUA","098","rua.com","rua@gmail","st009",R.drawable.u5));
    }
}
