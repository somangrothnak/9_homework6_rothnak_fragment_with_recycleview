package com.somee.rothnaksomang.homework6_fragmentlist_recycleview;

public class University {
    private String name;
    private String phone;
    private String website;
    private String mail;
    private String address;
    private Integer profile;

    public University(){}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getProfile() {
        return profile;
    }

    public void setProfile(Integer profile) {
        this.profile = profile;
    }

    public University(String name, String phone, String website, String mail, String address, int profile) {
        this.name = name;
        this.phone = phone;
        this.website = website;
        this.mail = mail;
        this.address = address;
        this.profile = profile;
    }
}
