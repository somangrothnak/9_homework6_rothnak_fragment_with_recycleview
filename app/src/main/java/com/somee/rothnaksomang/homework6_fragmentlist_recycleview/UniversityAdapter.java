package com.somee.rothnaksomang.homework6_fragmentlist_recycleview;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;

public class UniversityAdapter extends RecyclerView.Adapter<UniversityAdapter.UniversityViewHolder> {

    private Context context;
    private ArrayList<University> universities;
    private LayoutInflater inflater;
    private Callback callback;

    public UniversityAdapter(Context context,ArrayList<University> universities){
        this.context=context;
        this.universities=universities;
        inflater=LayoutInflater.from(context);
        callback= (Callback) context;
    }

    @NonNull
    @Override
    public UniversityViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view=inflater.inflate(R.layout.layout_item,null);
        return new UniversityViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull UniversityViewHolder universityViewHolder, final int i) {
        final University university=universities.get(i);
        universityViewHolder.tvName.setText(university.getName());
        universityViewHolder.tvCall.setText(university.getPhone());
        universityViewHolder.tvWebsite.setText(university.getWebsite());
        universityViewHolder.tvMail.setText(university.getMail());
        universityViewHolder.tvAddress.setText(university.getAddress());
        universityViewHolder.ivProfile.setImageResource(university.getProfile());
        universityViewHolder.ivProfileSmall.setImageResource(university.getProfile());
        universityViewHolder.cvItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.replaceFragment(universities.get(i));
                TextView tv=v.findViewById(R.id.tv_name);
                tv.setText(universities.get(i).getName());

            }
        });
    }

    @Override
    public int getItemCount() {
        return universities.size();
    }

    class UniversityViewHolder extends RecyclerView.ViewHolder{

        TextView tvName,tvCall,tvWebsite,tvMail,tvAddress;
        ImageView ivProfile,ivProfileSmall;
        CardView cvItem;

        public UniversityViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName=itemView.findViewById(R.id.tv_name);
            tvCall=itemView.findViewById(R.id.tv_call);
            tvWebsite=itemView.findViewById(R.id.tv_web);
            tvMail=itemView.findViewById(R.id.tv_mail);
            tvAddress=itemView.findViewById(R.id.tv_address);
            ivProfile=itemView.findViewById(R.id.iv_profile_big);
            cvItem=itemView.findViewById(R.id.cv_item_university);
            ivProfileSmall=itemView.findViewById(R.id.iv_profile_small);
        }
    }
}
